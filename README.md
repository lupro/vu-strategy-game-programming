# README #

### How do I get set up? ###

* Pull the project
* Import it in Eclipse
* Copy triplea.jar into the lib folder (you get the jar from tripleA in the bin folder). If the lib folder doesn't exist, create a new one in the project.
* Set up Builing Path
* Right click on the project and select Properties
* Select Run/Debug Settings and click on New... on the right side
* Select Java Application
* Set as Main class 'games.strategy.engine.framework.GameRunner'
* Click Apply and Ok
* Finished, now the engine should start when you press the play button

The development takes place in go_ai
The main AI class is MyAI.java