package go_ai;

import games.strategy.engine.gamePlayer.IGamePlayer;
import games.strategy.grid.go.Go;
import games.strategy.grid.go.player.GoPlayer;
import games.strategy.grid.go.player.RandomAI;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Adds your own AI to selectable AIs
 *
 */
public class GoWithAI extends Go {

	private static final long serialVersionUID = 2560802787325950593L;
	public static final String HUMAN_PLAYER_TYPE = "Human";
	public static final String RANDOM_COMPUTER_PLAYER_TYPE = "Random AI";
	public static final String MY_AI = "My AI";
	@Override
	public String[] getServerPlayerTypes() {
		return new String[] { HUMAN_PLAYER_TYPE, RANDOM_COMPUTER_PLAYER_TYPE,
				MY_AI };
	}

	@Override
	public Set<IGamePlayer> createPlayers(final Map<String, String> playerNames) {
		final Set<IGamePlayer> iplayers = new HashSet<IGamePlayer>();
		for (final String name : playerNames.keySet()) {
			final String type = playerNames.get(name);

			if (type.equals(HUMAN_PLAYER_TYPE)
					|| type.equals(CLIENT_PLAYER_TYPE)) {
				final GoPlayer player = new GoPlayer(name, type);
				iplayers.add(player);
			} else if (type.equals(RANDOM_COMPUTER_PLAYER_TYPE)) {
				final RandomAI ai = new RandomAI(name, type);
				iplayers.add(ai);
			} else if (type.equals(MY_AI)) {
				final go_ai.MyAI ai = new go_ai.MyAI(name, type);
				iplayers.add(ai);

			}  else {
				throw new IllegalStateException("Player type not recognized:"
						+ type);
			}
		}

		return iplayers;
	}


}
