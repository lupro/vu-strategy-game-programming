package go_ai;

import games.strategy.engine.data.GameData;
import games.strategy.engine.data.PlayerID;
import games.strategy.grid.go.delegate.remote.IGoPlayDelegate;
import games.strategy.grid.player.GridAbstractAI;

public class MyAI extends GridAbstractAI {

	public MyAI(String name, String type) {
		super(name, type);
	}

	/**
	 * Gets called by the engine every turn
	 */
	@Override
	protected void play() {

		final IGoPlayDelegate playDel = (IGoPlayDelegate) this.getPlayerBridge().getRemoteDelegate();
		final PlayerID me = getPlayerID();
		final GameData data = getGameData();
	

	}
	
}